(ns plf04.core)

(defn stringE-1
  [xs]
  (letfn [(f [x ys]
            (if (empty? x)
              (if (and (>= ys 1) (<= ys 3))
                true
                false)
              (if (= \e (first x))
                (f (rest x) (inc ys))
                (f (rest x) ys))))]
    (f xs 0)))

(stringE-1 "Hello")
(stringE-1 "Heelle")
(stringE-1 "Heelele")
(stringE-1 "Hll")
(stringE-1 "e")
(stringE-1 "")

(defn stringE-2
  [xs ys]
  (letfn [(f [x acc]
            (if (empty? x)
              (if (and (>= acc 1) (<= acc 3))
                true
                false)
              (if (and (= \e (first x)))
                (f (rest x) (inc acc))
                (f (rest x) acc))))]
    (f xs ys)))

(stringE-2 "Hello" 0)
(stringE-2 "Heelle" 0)
(stringE-2 "Heelele" 0)
(stringE-2 "Hll" 0)
(stringE-2 "e" 0)
(stringE-2 "" 0)


(defn string-times-1
  [xs ys]
  (letfn [(f [x y]
            (if (or (neg? (count x)) (= y 0))
              ""
              (if (<= y 1)
                x
                (str x (f x (dec y))))))]
    (f xs ys)))

(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

(defn string-times-2
  [xs ys]
  (letfn [(f [x y acc]
            (if (or (neg? (count x)) (zero? y))
              acc
              (if (<= y 0)
                x
                (str x (f x (dec y) acc)))))]
    (f xs ys "")))

(string-times-2 "Hi" 2)
(string-times-2 "Hi" 3)
(string-times-2 "Hi" 1)
(string-times-2 "Hi" 0)
(string-times-2 "Hi" 5)
(string-times-2 "Oh Boy!" 2)
(string-times-2 "x" 4)
(string-times-2 "" 4)
(string-times-2 "code" 2)
(string-times-2 "code" 3)

(defn front-times-1
  [xs ys]
  (letfn [(f [x y]
            (if (or (neg? (count x)) (zero? y))
              ""
              (if (> (count x) 3)
                (f (subs x 0 3) y)
                (str x (f x (dec y))))))]
    (f xs ys)))

(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)


(defn front-times-2
  [xs ys]
  (letfn [(f [x y acc]
            (if (or (neg? (count x)) (zero? y))
              acc
              (if (> (count x) 3)
                (f (subs x 0 3) y acc)
                (str x (f x (dec y) acc)))))]
    (f xs ys "")))

(front-times-2 "Chocolate" 2)
(front-times-2 "Chocolate" 3)
(front-times-2 "Abc" 3)
(front-times-2 "Ab" 4)
(front-times-2 "A" 4)
(front-times-2 "" 4)
(front-times-2 "Abc" 0)


(defn countxx-1
  [xs]
  (letfn [(f [x y]
            (if (empty? x)
              y
              (if (and (= \x (first x)) (= \x (first (rest x))))
                (f (rest x) (inc y))
                (f (rest x) y))))]
    (f xs 0)))

(countxx-1 "abcxx")
(countxx-1 "xxx")
(countxx-1 "xxxx")
(countxx-1 "abc")
(countxx-1 "Hello there")
(countxx-1 "Hexxo thxxe")
(countxx-1 "")
(countxx-1 "Kittens")
(countxx-1 "Kittensxxx")

(defn countxx-2
  [xs]
  (letfn [(f [x acc]
            (if (empty? x)
              x
              (if (and (= \x (first x)) (= \x (first (rest x))))
                (f (rest x) (inc acc))
                (f (rest x) acc))))]
    (f xs 1)))

(countxx-2 "abcxx")
(countxx-2 "xxx")
(countxx-2 "xxxx")
(countxx-2 "abc")
(countxx-2 "Hello there")
(countxx-2 "Hexxo thxxe")
(countxx-2 "")
(countxx-2 "Kittens")
(countxx-2 "Kittensxxx")



(defn string-splosion-1
  [xs]
  (letfn [(f [y]
            (if (<= (count y) 1)
              y
              (apply str (f (subs y 0 (- (count y) 1))) y)))]
    (f xs)))

(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "there")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

(defn string-splosion-2
  [xs]
  (letfn [(f [x acc]
            (if (== 0 (count x))
              acc
              (f (subs x 0 (- (count x) 1)) (str x acc))))]
    (f xs "")))

(string-splosion-2 "Code")

(string-splosion-2 "abc")
(string-splosion-2 "ab")
(string-splosion-2 "x")
(string-splosion-2 "fade")
(string-splosion-2 "there")
(string-splosion-2 "Kitten")
(string-splosion-2 "Bye")
(string-splosion-2 "Good")
(string-splosion-2 "Bad")


(defn array123-1
  [xs]
  (letfn [(f [ys]
            (if (or (empty? ys) (< (count ys) 3))
              false
              (if (and (= (first ys) 1)
                       (= (first (rest ys)) 2)
                       (= (first (rest (rest ys))) 3))
                true
                (f (rest ys)))))]
    (f xs)))

(array123-1 [1,1,2,3,1])
(array123-1 [1,1,2,4,1])
(array123-1 [1,1,2,3,1])
(array123-1 [1,1,2,1,2,3])
(array123-1 [1,1,2,1,2,1])
(array123-1 [1,2,3,1,2,3])
(array123-1 [1,2,3])
(array123-1 [1,1,1])
(array123-1 [1,2])
(array123-1 [1])
(array123-1 [])

(defn array123-2
  [xs]
  (letfn [(f [ys acc]
            (if (and (empty? ys) (< acc 3))
              false
              (if (= (first ys) acc)
                (if (= acc 3)
                  true
                  (if (< (count (rest ys)) 2)
                    false
                    (f (rest ys) (inc acc))))
                (if (> (first ys) 1)
                  (f (rest ys) 1)
                  (f ys 1)))))]
    (f xs 1)))

(array123-2 [1,1,2,3,1])
(array123-2 [1,1,2,4,1])
(array123-2 [1,1,2,3,1])
(array123-2 [1,1,2,1,2,3])
(array123-2 [1,1,2,1,2,1])
(array123-2 [1,2,3,1,2,3])
(array123-2 [1,2,3])
(array123-2 [1,1,1])
(array123-2 [1,2])
(array123-2 [1])
(array123-2 [])


(defn stringX-1
  [xs]
  (letfn [(f [x y]
            (if (empty? x)
              ""
              (if (zero? y)
                (str (first x) (f (rest x) (inc y)))
                (if (and (> (count x) 1) (= \x (first x)))
                  (f (rest x) (inc y))
                  (str (first x) (f (rest x) (inc y)))))))]
    (f xs 0)))

(stringX-1 "xxHxix")
(stringX-1 "abxxxcd")
(stringX-1 "xabxxxcdx")
(stringX-1 "xKittenx")
(stringX-1 "Hello")
(stringX-1 "xx")
(stringX-1 "x")
(stringX-1 "")

(defn stringx-2
  [xs]
  (letfn [(f [x y acc]
            (if (empty? x)
              acc
              (if (== 0 y)
                (str (first x) (f (rest x) (inc y) (str acc)))
                (if (and (> (count x) 1) (= \x (first x)))
                  (f (rest x) (inc y) (str acc))
                  (str (first x) (f (rest x) (inc y) acc))))))]
    (f xs 0 "")))

(stringx-2 "xxHxix")
(stringx-2 "abxxxcd")
(stringx-2 "xabxxxcdx")
(stringx-2 "xKittenx")
(stringx-2 "Hello")
(stringx-2 "xx")
(stringx-2 "x")
(stringx-2 "")



(defn altPairs-1
  [xs]
  (letfn [(f [x y]
            (if (or (= (count x) y) (empty? x))
              ""
              (if (or (zero? 0) (= y 1) (= y 4) (= y 5) (= y 8) (= y 9))
                (str (subs x y (inc y)) (f xs (inc y)))
                (f xs (inc y)))))]
    (f xs 0)))

(altPairs-1 "kitten")
(altPairs-1 "Chocolate")
(altPairs-1 "CodingHorror")
(altPairs-1 "yak")
(altPairs-1 "ya")
(altPairs-1 "")
(altPairs-1 "ThisThatTheOther")

(defn altPairs-2
  [xs]
  (letfn [(f [x y acc]
            (if (or (= (count x) y) (empty? x))
              acc
              (if (or (zero? 0) (= y 1) (= y 4) (= y 5) (= y 8) (= y 9))
                (f x (inc y) (str acc (subs x y (inc y))))
                (f x (inc y) acc))))]
    (f xs 0 "")))

(altPairs-2 "kitten")
(altPairs-2 "Chocolate")
(altPairs-2 "CodingHorror")
(altPairs-2 "yak")
(altPairs-2 "ya")
(altPairs-2 "")
(altPairs-2 "ThisThatTheOTher")


(defn stringYak-1
  [x]
  (letfn [(f [y z]
            (if (empty? y)
              ""
              (if (and (= (first y) \y) (= (first (rest y)) \a) (= (first (rest (rest y))) \k) (> (count y) 1))
                (f (rest (rest (rest y))) (inc z))
                (str (first y) (f (rest y) (inc z))))))]
    (f x 0)))
 
(stringYak-1 "yakpak")
(stringYak-1 "pakyak")
(stringYak-1 "yak123ya")
(stringYak-1 "yak")
(stringYak-1 "yakxxxyak")
(stringYak-1 "HiyakHi")
(stringYak-1 "xxxyakyyyakzzz")

(defn stringYak-2
  [xs]
  (letfn [(f [x y acc]
            (if (empty? x)
              acc
              (if (and (= (first (rest (rest x))) \k) (= (first (rest x)) \a) (= (first x) \y) (> (count x) 1))
                (f (rest (rest (rest x))) (inc y) acc)
                (f (rest x) (inc y) (str acc (first x))))))]
    (f xs 0 "")))

(stringYak-2 "yakpak")
(stringYak-2 "pakyak")
(stringYak-2 "yak123ya")
(stringYak-2 "yak")
(stringYak-2 "yakxxxyak")
(stringYak-2 "HiyakHi")
(stringYak-2 "xxxyakyyyakzzz")


(defn has271-1
  [xs]
  (letfn [(f [ys]
            (if (or (<= (count ys) 2) (empty? ys))
              false
              (if (and
                   (== (first (rest ys)) (+ (first ys) 5))
                   (<= (if (pos? (- (first (rest (rest ys))) (dec (first ys))))
                         (- (first (rest (rest ys))) (dec (first ys)))
                         (* -1 (- (first (rest (rest ys))) (dec (first ys))))) 2))
                true
                (f (rest ys)))))]
    (f xs)))

(has271-1 [1,2,7,1])
(has271-1 [1, 2, 8, 1])
(has271-1 [2, 7, 1])
(has271-1 [3, 8, 2])
(has271-1 [2, 7, 3])
(has271-1 [2, 7, 4])
(has271-1 [2, 7, -1])
(has271-1 [2, 7, -2])
(has271-1 [4, 5, 3, 8, 0])
(has271-1 [2, 7, 5, 10, 4])
(has271-1 [2, 7, -2, 4, 9, 3])
(has271-1 [2, 7, 5, 10, 1])
(has271-1 [2, 7, -2, 4, 10, 2])
(has271-1 [1, 1, 4, 9, 0])
(has271-1 [1, 1, 4, 9, 4, 9, 2])

(defn has271-2
  [xs]
  (letfn [(f [ys acc]
            (if (or (<= (count ys) 2) (empty? ys) (zero? acc))
              false
              (if (and
                   (== (first (rest ys)) (+ (first ys) 5))
                   (<= (if (pos? (- (first (rest (rest ys))) (dec (first ys))))
                         (- (first (rest (rest ys))) (dec (first ys)))
                         (* -1 (- (first (rest (rest ys))) (dec (first ys))))) 2))
                true
                (f (rest ys) acc))))]
    (f xs 3)))

(has271-2 [1,2,7,1])
(has271-2 [1, 2, 8, 1])
(has271-2 [2, 7, 1])
(has271-2 [3, 8, 2])
(has271-2 [2, 7, 3])
(has271-2 [2, 7, 4])
(has271-2 [2, 7, -1])
(has271-2 [2, 7, -2])
(has271-2 [4, 5, 3, 8, 0])
(has271-2 [2, 7, 5, 10, 4])
(has271-2 [2, 7, -2, 4, 9, 3])
(has271-2 [2, 7, 5, 10, 1])
(has271-2 [2, 7, -2, 4, 10, 2])
(has271-2 [1, 1, 4, 9, 0])
(has271-2 [1, 1, 4, 9, 4, 9, 2])
